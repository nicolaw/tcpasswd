# Changelog

## v0.3.0 (2024-08-14)
### Fixed
- Reverted erroneous -gmlevel max value 4 vs 3 change

## v0.2.0 (2024-08-06)
### Changed
- Changed -gmlevel to accept a value of up to 4 

## v0.1.5 (2024-07-31)
### Fixed
- Refreshed vendored packages and rebuilt for Golang 1.22

## v0.1.4 (2022-02-19)
### Added
- Add -gmlevel argument to optionally set security level

## v0.1.3 (2022-02-10)
### Added
- Compress multi-arch tcpasswd container !6

## v0.1.2 (2022-02-08)
### Added
- Documentation tweaks
- Add Docker image !5

## v0.1.1 (2022-02-08)
### Fixed
- Silence false positives in CI tests

## v0.1.0 (2022-02-08)
### Added
- Add basic SQL, YAML, JSON and TOML outputs !4
- Update readme !3
- Add release job !2

## v0.0.1 (2022-02-06)
### Added
- Initial development release

