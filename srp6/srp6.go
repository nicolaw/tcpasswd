package srp6

import (
	"crypto/rand"
	"crypto/sha1" //#nosec G505 -- Required as defined by SRP6 specificiation
	"encoding/hex"
	"fmt"
	"math/big"
)

var (
	// Constants used by the WoW SRP6 implementation.
	G = big.NewInt(7)
	N = getModulus()
)

// `getModulus` returns the constant big integer modulus that WoW uses
// in its SRP6 implementation. It would panic if the hardcoded constant
// hex string literal ever failed to parse as hex.
func getModulus() big.Int {
	n, err := hex.DecodeString("894B645E89E1535BBDAD5B8B290650530801B18EBFBF5E8FAB3C82872A3E9BB7")
	if err != nil {
		panic("Invalid verifier constant in program source")
	}

	var bign big.Int
	bign.SetBytes(n)
	return bign
}

// `toUpperAZOnly` returns the input string with ONLY the ASCII characters a-z
// upper-cased. This is in line with the action of the TrinityCore server; see
// https://github.com/TrinityCore/TrinityCore/blob/master/src/common/Utilities/Util.h#L262
// and `isBasicLatinCharacter()`.
func ToUpperAZOnly(input string) string {
	tmp := []rune(input)
	for i, r := range tmp {
		if r >= 'a' && r <= 'z' {
			tmp[i] = r - ('a' - 'A')
		}
	}
	return string(tmp)
}

// `reverse` simply in-place reverses the order of the given byte slice.
// In this module, this is used to convert little-endian hash digests
// to and from big-endian inputs to Go's big integer libraries.
func reverse(input []byte) {
	for i := 0; i < len(input)/2; i++ {
		r := len(input) - i - 1
		input[i], input[r] = input[r], input[i]
	}
}

// `getSalt` returns 32 bytes of random data to use as salt.
func GetSalt() ([]byte, error) {
	data := make([]byte, 32)
	_, err := rand.Read(data)
	return data, err
}

// `CalculateSRP6Verifier` returns the 32-byte SRP6 verifier bytestring for
// the given UTF-8 username and password strings plus the given 32-byte salt,
// or returns any error encountered.
func CalculateSRP6Verifier(username string, password string, salt []byte) (
	[]byte, error) {
	if len(salt) != 32 {
		return nil, fmt.Errorf("the provided salt data was %d bytes "+
			"rather than the expected 32 bytes", len(salt))
	}

	// Calculate the SHA1 sum of the uppercased string <username>:<password>.
	h1text := ToUpperAZOnly(username + ":" + password)
	h1 := sha1.Sum([]byte(h1text)) //#nosec G401 -- Required as defined by SRP6 specificiation

	// Append the bytes of h1 to the salt, and calculate the SHA1 sum of that.
	h2bytes := append(salt, h1[:]...)
	h2 := sha1.Sum(h2bytes) //#nosec G401 -- Required as defined by SRP6 specificiation

	// Convert h2 to bigendian, and construct a big integer from it.
	var h2int big.Int
	reverse(h2[:])
	h2int.SetBytes(h2[:])

	// Calculate (g ^ h2) % N
	var verifierInt big.Int
	verifierInt.Exp(G, &h2int, &N)

	// Convert the resulting big integer back to a little-endian
	// verifier bytestring.
	verifier := verifierInt.Bytes()
	reverse(verifier)

	// Ensure that the verifier bytestring is exactly 32 bytes in length,
	// zeropadding (to the right, since littleendian) if necessary.
	if len(verifier) > 32 {
		return nil, fmt.Errorf("the calculated verifier was %d bytes which "+
			"is longer than the expected maximum of 32 bytes", len(verifier))
	}

	verifier32 := make([]byte, 32)
	copy(verifier32, verifier)
	return verifier32, nil
}
