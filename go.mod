module gitlab.com/nicolaw/tcpasswd

go 1.22

require (
	github.com/pelletier/go-toml/v2 v2.2.2
	gopkg.in/yaml.v2 v2.4.0
)
