package main

import (
	"encoding/hex"
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"strconv"
	"strings"

	"github.com/pelletier/go-toml/v2"
	"gitlab.com/nicolaw/tcpasswd/srp6"
	"gopkg.in/yaml.v2"
)

func main() {
	var err error

	var username string
	var password string
	var saltText string
	var outputFmt string = "sql"
	var gmLevel int

	// TODO: https://lightstep.com/blog/getting-real-with-command-line-arguments-and-goflags/
	//       https://pkg.go.dev/gopkg.in/go-playground/validator.v10
	//       Make argument parsing and validation less n00bish.

	flag.StringVar(&username, "username", "", "Username")
	flag.StringVar(&password, "password", "", "Password")
	flag.StringVar(&saltText, "salt", "", "32 byte hexadecimal salt (optional)")
	flag.StringVar(&outputFmt, "output", "sql", "Output format; json|yaml|toml|sql")
	flag.IntVar(&gmLevel, "gmlevel", 0, "Account security level; 0|1|2|3 (optional)")

	flag.Parse()

	required := []string{"username", "password"}
	seen := make(map[string]bool)
	flag.Visit(func(f *flag.Flag) {
		if len(strings.TrimSpace(f.Value.String())) > 0 {
			seen[f.Name] = true
		}
	})
	for _, req := range required {
		if !seen[req] {
			fmt.Fprintf(os.Stderr, "Missing required -%s argument.\n\n", req)
			flag.PrintDefaults()
			os.Exit(2) // the same exit code flag.Parse uses
		}
	}

	if gmLevel < 0 || gmLevel > 3 {
		fmt.Fprintf(os.Stderr, "Optional -gmlevel argument is not a valid value.\n\n")
		flag.PrintDefaults()
		os.Exit(2) // the same exit code flag.Parse uses
	}

	outputFmt = strings.ToLower(outputFmt)
	if outputFmt != "sql" && outputFmt != "json" && outputFmt != "yaml" && outputFmt != "toml" {
		fmt.Fprintf(os.Stderr, "Argument -output is not a valid value.\n\n")
		flag.PrintDefaults()
		os.Exit(2) // the same exit code flag.Parse uses
	}

	// Convert username and password to uppercase.
	username = srp6.ToUpperAZOnly(username)
	password = srp6.ToUpperAZOnly(password)

	out := make(map[string]string)
	out["username"] = username
	out["password"] = password

	if outputFmt == "sql" && seen["gmlevel"] {
		out["SecurityLevel"] = strconv.Itoa(gmLevel)
	}

	var salt []byte
	if len(saltText) > 0 {
		salt, err = hex.DecodeString(saltText)
		if err != nil || len(salt) != 32 {
			fmt.Fprintf(os.Stderr, "Optional -salt argument is not a valid 32 byte hex string.\n\n")
			flag.PrintDefaults()
			os.Exit(2) // the same exit code flag.Parse uses
		}
	} else {
		salt, err = srp6.GetSalt()
		if err != nil {
			panic(err)
		}
	}

	out["salt"] = fmt.Sprintf("%x", salt)

	verifier, err := srp6.CalculateSRP6Verifier(username, password, salt)
	if err != nil {
		fmt.Println(err)
		return
	}

	out["verifier"] = fmt.Sprintf("%x", verifier)

	// Print the results.
	printResults(out, outputFmt)
}

// `printResults` print results in the specified output format.
func printResults(out map[string]string, format string) {
	switch strings.ToLower(format) {
	case "json":
		b, err := json.Marshal(out)
		if err != nil {
			panic(err)
		}
		fmt.Println(string(b))

	case "yaml":
		b, err := yaml.Marshal(&out)
		if err != nil {
			panic(err)
		}
		fmt.Println(string(b))

	case "toml":
		b, err := toml.Marshal(out)
		if err != nil {
			panic(err)
		}
		fmt.Println(string(b))

	default:
		fmt.Println("START TRANSACTION;")
		fmt.Println("SET @id := NULL;")
		fmt.Printf("SELECT @id := id FROM account WHERE username = '%s';\n", out["username"])
		fmt.Printf("REPLACE INTO account (id, username, salt, verifier) VALUES (@id, '%s', X'%s', X'%s');\n", out["username"], out["salt"], out["verifier"])
		if len(out["SecurityLevel"]) > 0 {
			fmt.Printf("REPLACE INTO account_access (AccountID, SecurityLevel) VALUES ((SELECT id FROM account WHERE username = '%s'), %v);\n", out["username"], out["SecurityLevel"])
		}
		fmt.Println("COMMIT;")
	}
}
