# tcpasswd

Generate [SRP6](http://srp.stanford.edu/) salt and verifiers used by the
[TrinityCore project](https://github.com/TrinityCore/TrinityCore), allowing
simple scripted creation of new user accounts in the
[MySQL database](https://trinitycore.info/en/database/335/auth/account).

* [Pre-compiled binary releases](https://gitlab.com/nicolaw/tcpasswd/-/releases)
* [Pre-built Docker images](https://hub.docker.com/r/nicolaw/tcpasswd)
* [Official project source repository](https://gitlab.com/nicolaw/tcpasswd)
* [Feature requests and issues](https://gitlab.com/nicolaw/tcpasswd/-/issues)


## Synopsis

    $ tcpasswd --username johndoe --password hunter2
    START TRANSACTION;
    SET @id := NULL;
    SELECT @id := id FROM account WHERE username = 'JOHNDOE';
    REPLACE INTO account (id, username, salt, verifier) VALUES (@id, 'JOHNDOE', X'f9d3e0488d6cd5b52c53ea3375da17e5bb7a1124ed461025f1cc6af74be94321', X'114d72079585acc6bf8b126d46c90bd77490be6739a09123bbfc1c4c8d61326c');
    COMMIT;


## Usage

    $ tcpasswd -help
    Usage of tcpasswd:
    -output string
            Output format; json|yaml|toml|sql (default "sql")
    -password string
            Password
    -salt string
            32 byte hexadecimal salt (optional)
    -username string
            Username


## Example

An example SQL statement to create an account in the TrinityCore 3.3.5
`auth.account` table:

    $ tcpasswd --username johndoe --password hunter2 --output sql | mysql -P 3306 -D auth

Alternative output formats:

    $ tcpasswd --username johndoe --password hunter2 --output json
    {"password":"HUNTER2","salt":"922c852517c07aafd7644f519eb4ea3f6c37d60500a8db1ac1f008398279b420","username":"JOHNDOE","verifier":"87aa6569864a9b414083d9beb84f5b1468121c755aa783969a95989255434b19"}

    $ tcpasswd --username johndoe --password hunter2 --output yaml
    password: HUNTER2
    salt: e71f8e749078634fe2117a78ed4e4465daf6595da6e52702e4544c43d2abac84
    username: JOHNDOE
    verifier: 992f07c742b5f2a48b887b5035a0d659cb524856c0edd5945c0158aa40c5f13a

    $ tcpasswd --username johndoe --password hunter2 --output toml
    password = 'HUNTER2'
    salt = '799cf392b7007ec7ad397a62a724a1ca2c185c266417422d3a7db231602d2bf6'
    username = 'JOHNDOE'
    verifier = '775648711a51af08335b9a9ceb013a3d30d7ec18268269dfd6eb17f760ba9e08'


## Docker

A Docker image is published at `docker.io/nicolaw/tcpasswd:latest`. See
https://hub.docker.com/r/nicolaw/tcpasswd/tags for a list of all available
tagged revisions.

    $ docker run --rm nicolaw/tcpasswd --username johndoe --password hunter2


## License

This project is relased under the [MIT license](LICENSE).

