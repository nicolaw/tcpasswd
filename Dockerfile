# MIT License
# Copyright (c) 2022-2024 Nicola Worthington <nicolaw@tfb.net>
# https://gitlab.com/nicolaw/tcpasswd/

FROM golang:1.22-alpine3.20 AS build

WORKDIR /src

COPY vendor/ ./vendor/
COPY srp6/ ./srp6/
COPY *.go go.* ./

RUN go build -ldflags="-s -w" -o /tcpasswd  .

FROM scratch

COPY --from=build /tcpasswd /

USER 65534

ENTRYPOINT ["/tcpasswd"]

ARG BUILD_DATE
ARG VCS_REF

LABEL org.opencontainers.image.authors="Nicola Worthington <nicolaw@tfb.net>" \
      org.opencontainers.image.created="$BUILD_DATE" \
      org.opencontainers.image.title="nicolaw/tcpasswd" \
      org.opencontainers.image.description="TrinityCore account creation and password generator" \
      org.opencontainers.image.documentation="https://gitlab.com/nicolaw/tcpasswd/-/blob/main/README.md" \
      org.opencontainers.image.url="https://nicolaw.uk/trinitycore" \
      org.opencontainers.image.source="https://gitlab.com/nicolaw/tcpasswd/" \
      org.opencontainers.image.revision="$VCS_REF" \
      org.opencontainers.image.vendor="Nicola Worthington <nicolaw@tfb.net>" \
      org.opencontainers.image.licenses="MIT" \
      org.opencontainers.image.docker.cmd="docker run --rm -it nicolaw/tcpasswd -help"
